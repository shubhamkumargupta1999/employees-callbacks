function getDataByCompany(dataPath, companyName, cb) {
    try {
        const data = require(dataPath);
        setTimeout(() => {
            const dataByCompany = data.employees.filter((curr) => curr.company === companyName);

            cb(dataByCompany);
        }, 2000)
    }
    catch (err) {
        console.log(err);
    }
}

// getDataByCompany('./data.json', "Powerpuff Brigade", (dataByCompany) => {
//     console.log(dataByCompany);
// })

module.exports = getDataByCompany;