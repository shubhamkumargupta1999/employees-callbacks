function sortData(dataPath, cb) {
    try {
        const data = require(dataPath);
        setTimeout(() => {
            const sortedData = data.employees.sort((a, b) => {
                if (a.company < b.company) {
                    return -1;
                }

                else if (a.company > b.company) {
                    return 1;
                }
                else {
                    return a.id - b.id;
                }
            })

            const sortedObj = {};
            sortedObj["employees"] = sortedData;

            cb(sortedObj);
        }, 2000)
    }
    catch (err) {
        console.log(err);
    }
}

// sortData('./data.json', (sortedData) => {
//     console.log(sortedData);
// })

module.exports = sortData;