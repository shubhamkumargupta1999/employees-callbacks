const fs = require('fs');

function writeFile(data, fileName, cb) {
    fs.writeFile(fileName, JSON.stringify(data, null, 2), (err)=>{
        if (err) throw err;
        console.log(`Wrote ${fileName}`);
        cb();
    })

}

module.exports = writeFile;