function addBirth(dataPath, cb) {
    try {
        const data = require(dataPath);
        setTimeout(() => {

            const birthData = [];

            data.employees.forEach((curr) => {
                if (curr.id % 2 === 0) {
                    curr["birthday"] = new Date();
                }

                birthData.push(curr);
            });

            const birthDataObj = {};
            birthDataObj["employees"] = birthData;

            cb(birthDataObj);
        }, 2000)
    }
    catch (err) {
        console.log(err);
    }
}

// addBirth('./data.json', (birthDataObj) => {
//     console.log(birthDataObj);
// })

module.exports = addBirth;