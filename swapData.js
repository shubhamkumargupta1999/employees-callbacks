function swapData(dataPath, id1, id2, cb) {
    try {
        const data = require(dataPath);
        setTimeout(() => {
            let company1 = data.employees.find((curr) => curr.id === id1).company;
            let company2 = data.employees.find((curr) => curr.id === id2).company;

            const swappedData = [];

            data.employees.forEach((curr) => {
                if (curr.id === id1) {
                    curr.company = company2;
                }

                if (curr.id === id2) {
                    curr.company = company1;
                }

                swappedData.push(curr);


            });

            const swappedDataObj = {};
            swappedDataObj["employees"] = swappedData;

            cb(swappedDataObj);
        }, 2000)
    }
    catch (err) {
        console.log(err);
    }
}

// swapData('./data.json', 92, 93, (swappedData) => {
//     console.log(swappedData);
// })

module.exports = swapData;