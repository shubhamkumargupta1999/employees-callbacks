function getRemoveByID(dataPath, idValue, cb) {
    try {
        const data = require(dataPath);
        setTimeout(() => {
            const removeByID = data.employees.filter((curr) => curr.id !== idValue);

            const obj = {};
            obj["employees"] = removeByID;

            cb(obj);
        }, 2000)
    }
    catch (err) {
        console.log(err);
    }
}

// getRemoveByID('./data.json', 2, (removeByIDObj) => {
//     console.log(removeByIDObj);
// })

module.exports = getRemoveByID;