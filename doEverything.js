const dataPath = './data.json';
const writeFile = require('./writeToFile');

const getDataByID = require('./getDataById');
const getDataByGroup = require('./getDataByGroup');
const getDataByCompany = require('./getDataByCompany');
const getRemoveByID = require('./getRemoveByID');
const sortData = require('./sortData');
const swapData = require('./swapData');
const addBirth = require("./addBirth");


getDataByID('./data.json', [2, 13, 23], (dataById) => {
    writeFile(dataById, './output/1.-dataByID.json', () => {
        getDataByGroup('./data.json', (dataByGroup) => {
            writeFile(dataByGroup, './output/2.-dataByGroup.json', () => {
                getDataByCompany('./data.json', "Powerpuff Brigade", (dataByCompany) => {
                    writeFile(dataByCompany, './output/3.-dataByCompany.json', () => {
                        getRemoveByID('./data.json', 2, (removeByIDObj) => {
                            writeFile(removeByIDObj, './output/4-removeByIDObj.json', () => {
                                sortData('./data.json', (sortedData) => {
                                    writeFile(sortedData, './output/5.-sortedData.json', () => {
                                        swapData('./data.json', 92, 93, (swappedData) => {
                                            writeFile(swappedData, './output/6.-swappedData.json', () => {
                                                addBirth('./data.json', (birthDataObj) => {
                                                    writeFile(birthDataObj, './output/7.-addBirth.json', () => {
                                                        console.log("Completed all tasks.");
                                                    })
                                                })
                                            })
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })
})



