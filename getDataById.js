function getDataByID(dataPath, arr, cb) {
    try {
        const data = require(dataPath);
        setTimeout(() => {
            const dataById = data.employees.filter((curr) => arr.includes(curr.id));

            cb(dataById);
        }, 2000);

    }
    catch (err) {
        console.log(err);
    }
}

// getDataByID('./data.json', [2, 13, 23], (dataById) => {
//     console.log(dataById);
// })

module.exports = getDataByID;