function getDataByGroup(dataPath, cb) {
    try {
        const data = require(dataPath);
        setTimeout(() => {
            const dataByGroup = data.employees.reduce((acc, curr) => {
                acc[curr.company] = acc[curr.company] || [];
                acc[curr.company].push(curr);

                return acc;
            }, {});

            cb(dataByGroup);
        }, 2000)
    }
    catch (err) {
        console.log(err);
    }
}

// getDataByGroup('./data.json', (dataByGroup) => {
//     console.log(dataByGroup);
// })

module.exports = getDataByGroup;